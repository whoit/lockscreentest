//
//  ViewController.swift
//  LockScreenTest
//

import UIKit
import MediaPlayer

let avSession = AVAudioSession.sharedInstance()
let commandCenter = MPRemoteCommandCenter.shared()
let nowPlayingInfoCenter = MPNowPlayingInfoCenter.default()
//let notificationCenter = NotificationCenter.default
var isPlaying = false
let myMP:MPMusicPlayerController = MPMusicPlayerController.applicationMusicPlayer

class ViewController: UIViewController {
    @IBOutlet weak var But_Play: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            // Configure the audio session for playback
            try avSession.setCategory(AVAudioSession.Category.playback,
                                      mode: AVAudioSession.Mode.default,
                                      options: [])
            try avSession.setActive(true)
        } catch let error as NSError {
            print("Failed to set the audio session category and mode: \(error.localizedDescription)")
        }
        
        // Do any additional setup after loading the view, typically from a nib.
        let qry = MPMediaQuery.songs()
        myMP.setQueue(with: qry)
        
        //self.becomeFirstResponder()
        //setupNowPlayingInfo()
    }

    func setupNowPlayingInfo(){
        //        let image = UIImage(named: "artwork")!
        //        let mediaArtwork = MPMediaItemArtwork(boundsSize: image.size) { (size: CGSize) -> UIImage in
        //            return image
        //        }
        let nowPlayingInfo: [String: Any] = [
            MPMediaItemPropertyArtist: "Pink Floyd",
            MPMediaItemPropertyTitle: "Wish You Were Here",
            MPMediaItemPropertyAlbumTitle: "Wish You Were Here",
            MPMediaItemPropertyPlaybackDuration: 10.0,
            MPNowPlayingInfoPropertyPlaybackRate: 1.0
            //MPMediaItemPropertyArtwork: mediaArtwork,
        ]
        nowPlayingInfoCenter.nowPlayingInfo = nowPlayingInfo
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
    }

    @IBAction func But_Play_Tap(_ sender: Any) {
        if isPlaying {
            isPlaying = false
            myMP.pause()
        } else {
            isPlaying = true
            myMP.play()
            setupNowPlayingInfo()
        }
    }
}
